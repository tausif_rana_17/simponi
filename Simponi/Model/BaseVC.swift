//
//  BaseVC.swift
//  Simponi
//
//  Created by suresh on 10/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit
import ReachabilitySwift
import Alamofire
import SwiftyJSON

class BaseVC: UIViewController
{


    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    func checkInternet() -> Bool {
        let reachability: Reachability
        reachability = Reachability()!
        let networkStatus: Int = reachability.currentReachabilityStatus.hashValue
        return networkStatus != 0
    }
    
    func showNormalAlert(strMessage: String) {
        let alert = UIAlertController(title: Constants.Keys.APPName, message: strMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
  /*  let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        
        return SessionManager(configuration: configuration)
    }()
    */
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
