//
//  Constants.swift
//  Simponi
//
//  Created by suresh on 10/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import Foundation
import UIKit

struct Constants
{
    struct URLS
    {
        static let BASE_URL = "https://www.simponivitality.ie/index.php/api/"
        
        static var loginURL: String
        {
            return BASE_URL + "login"
        }
        static var forgotPassword: String
        {
            return BASE_URL + "forgot-password"
        }
    }
    
    struct Keys
    {
        static let APPName = "SIMPONI"
        static let INTERNET_UNAVAILABLE = "Please check your internet connection and try again."
        
    }
    
    struct SESSION
    {
        static let CUSTOMERID = "CUSTOMERID"
        static let CUSTOMERNAME = "NAME"
        static let CUSTOMEREMAIL = "EMAIL"
        static let TOKENID = "TOKENID"
        static let PATIENT_TYPE = "LOGINTYPE"
        static let GASTRO = "Gastro"
        static let RHEUM = "Rheum"


    }
    
    
    struct Messages
    {
        static let EMAIL_REQUIRED = "Please enter email id"
        static let PASSWORD_REQUIRED = "Please enter password"
    }
    
    struct STORYBOARD
    {
        static let MAIN =  "Main"
        
        struct CLASS_NAME
        {
            static let LoginVC =  "LoginVCID"
            static let MenuVC =  "MenuVCID"
            static let ForgotPasswordVC =  "ForgotPasswordVCID"
            static let AboutSimponiVC =  "AboutSimponiVCID"
            static let FAQVc =  "FAQVcID"
            static let InjectVC =  "InjectVCID"
            static let DosingVC =  "DosingVCID"
            static let StorageDisposalVC =  "StorageDisposalVCID"
            static let SafetyVC =  "SafetyVCID"
            static let MyConditionVC =  "MyConditionVCID"
            static let PsoriaticVC =  "PsoriaticVCID"
            static let LifestyleVC =  "LifestyleVCID"
            static let HelpfulvideosVC =  "HelpfulvideosVCID"
            static let MembersupportVC =  "MembersupportVCID"
            static let TravelpackVC =  "TravelpackVCID"
            static let TextreminderVC =  "TextreminderVCID"
        }
    }
    
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_7 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_7P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
    }
    
    struct Description
    {
        static let LIFESTYLE_NOTE = "Welcome to your Vitality Lifestyle homepage. Here you will find the information and advice you need to live the way you want, without letting your condition hold you back. From healthy, tasty recipes, to finding the right fitness regime for you, to achieving that piece of mind that's essential to happiness, this Lifestyle section will be your go-to-guide for getting active in every sense!"
    }

    
}

