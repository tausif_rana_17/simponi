//
//  Menu.swift
//  Simponi
//
//  Created by suresh on 10/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import Foundation

internal class Menu {
    internal let id: String
    internal let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
