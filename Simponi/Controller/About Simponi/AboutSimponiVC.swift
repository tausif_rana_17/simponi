//
//  AboutSimponiVC.swift
//  Simponi
//
//  Created by suresh on 12/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit

class AboutSimponiVC: UIViewController , UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    let kHeaderSectionTag: Int = 6900;
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblInjectClick: UILabel!
    @IBOutlet weak var lblDosingClick: UILabel!
    @IBOutlet weak var lblStorageClick: UILabel!
    @IBOutlet weak var lblSafetyClick: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 132.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        sectionNames = [ "\tWhat is SIMPONI?", "\tHow does it works?", "\tWhy your doctor prescribed SIMPONI?" ];
        sectionItems = [ ["SIMPONI® (golimumab) is a prescription medicine. SIMPONI® can lower your ability to fight infections. There are reports of serious infections caused by bacteria, fungi, or viruses that have spread throughout the body, including tuberculosis (TB) and histoplasmosis. Some of these infections have been fatal. Your doctor will test you for TB before starting SIMPONI® and will monitor you for signs of TB during treatment. Tell your doctor if you have been in close contact with people with TB. Tell your doctor if you have been in a region (such as the Ohio and Mississippi River Valleys and the Southwest) where certain fungal infections like histoplasmosis or coccidioidomycosis are common."],
                         ["Treatment with SIMPONI® begins with 3 starter injections: 2 injections on your first day of treatment, followed by 1 injection 2 weeks later. After these 3 starter injections, SIMPONI® requires just 1 injection every 4 weeks. If your doctor decides that you or your caregiver can give your injections at home, you will need to be trained on the proper way to self-inject SIMPONI® directly under the skin."],
                         ["SIMPONI® is intended for use under the guidance and supervision of a physician. Patients may self-inject with SIMPONI® after physician approval and proper training.Prior to initiating SIMPONI® and periodically during therapy, patients should be evaluated for active tuberculosis and tested for latent infection. Prior to initiating SIMPONI®, patients should be tested for hepatitis B viral infection."]
        ];
        self.tableView!.tableFooterView = UIView()
        
        self.imgView.layer.cornerRadius = 5
        self.imgView.layer.shadowColor = UIColor.gray.cgColor
        self.imgView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.imgView.layer.shadowOpacity = 0.3
        self.imgView.layer.borderColor = UIColor.lightText.cgColor
        self.imgView.layer.borderWidth = 1
        
        
    
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.lblInjectClickTap))
        lblInjectClick.isUserInteractionEnabled = true
        lblInjectClick.addGestureRecognizer(gestureRecognizer)

        let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(AboutSimponiVC.lblDosingClickTap))
        lblDosingClick.isUserInteractionEnabled = true
        lblDosingClick.addGestureRecognizer(gestureRecognizer1)
        
  
        let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(AboutSimponiVC.lblStorageClickTap))
        lblStorageClick.isUserInteractionEnabled = true
        lblStorageClick.addGestureRecognizer(gestureRecognizer2)
        
        let gestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(AboutSimponiVC.lblSafetyClickTap))
        lblSafetyClick.isUserInteractionEnabled = true
        lblSafetyClick.addGestureRecognizer(gestureRecognizer3)

    }
    
    @objc func lblInjectClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.InjectVC) as! InjectVC
        
        self.present(loginVC, animated: true, completion: nil)
    }
    
    @objc func lblDosingClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.DosingVC) as! DosingVC
          self.present(loginVC, animated: true, completion: nil)
    }
    
    @objc func lblStorageClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.StorageDisposalVC) as! StorageDisposalVC
          self.present(loginVC, animated: true, completion: nil)
    }
    
    @objc func lblSafetyClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.SafetyVC) as! SafetyVC
         self.present(loginVC, animated: true, completion: nil)

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = "Retrieving data....."
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = .center;
            messageLabel.font = UIFont(name: "HelveticaNeue", size: 16.0)!
            messageLabel.sizeToFit()
            self.tableView.backgroundView = messageLabel;
        }
        return 0
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
   /* func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = UIColor.red
        return headerView
    }*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            return self.sectionNames[section] as? String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.colorWithHexString(hexStr: "#2F5173")
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont(name: "OpenSans-Semibold", size: 15)!
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }

        let theImageView = UIImageView(frame: CGRect(x: 5, y: 13, width: 18, height: 18));
        
        theImageView.image = UIImage(named: "Chevron-Dn-Wht")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(FAQVc.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       // return 111
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.textLabel?.font = UIFont(name:"OpenSans-Semibold", size:15)
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.text = section[indexPath.row] as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tableView!.beginUpdates()
            self.tableView!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
