//
//  InjectDetailSubview.swift
//  Simponi
//
//  Created by suresh on 13/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import AVKit

class InjectDetailSubview: UIViewController , AVPlayerViewControllerDelegate , UITableViewDelegate, UITableViewDataSource
{

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var myVideoview: UIView!

    var playerItem:AVPlayerItem?
    var player:AVPlayer?
    
    var arrItems: Array<Any> = []
    let cellSpacingHeight: CGFloat = 3
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.myVideoview.layer.shadowColor = UIColor.gray.cgColor
        self.myVideoview.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.myVideoview.layer.shadowOpacity = 0.3
        self.myVideoview.layer.borderColor = UIColor.lightText.cgColor
        self.myVideoview.layer.borderWidth = 1
        
        tableView.estimatedRowHeight = 132.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        
        arrItems = [ "Simponi Injection Guide", "Disposing Simponi"];
        
        let playerController = AVPlayerViewController()
        playerController.delegate = self
        
        let videoURL = NSURL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        let player = AVPlayer(url: videoURL! as URL)
        playerController.player = player
        self.addChildViewController(playerController)
        self.myVideoview.addSubview(playerController.view)
        playerController.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 17 , height: 200)
        playerController.showsPlaybackControls = true
        player.play()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        
        cell.myImageview.layer.cornerRadius = 0
        cell.myImageview.layer.masksToBounds = true
        cell.myImageview.layer.shadowColor = UIColor.gray.cgColor
        cell.myImageview.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.myImageview.layer.shadowOpacity = 0.5
        cell.myImageview.layer.borderColor = UIColor.lightGray.cgColor
        cell.myImageview.layer.borderWidth = 1
        
        
        cell.myDisplayview.layer.cornerRadius = 5
        cell.myDisplayview.layer.masksToBounds = true
        cell.myDisplayview.layer.shadowColor = UIColor.gray.cgColor
        cell.myDisplayview.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.myDisplayview.layer.shadowOpacity = 0.5
        cell.myDisplayview.layer.borderColor = UIColor.lightGray.cgColor
        cell.myDisplayview.layer.borderWidth = 1
        
        let str = arrItems[indexPath.section] as? String
        cell.lblName?.text = str
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("You tapped cell number \(indexPath.section).")
    }
    
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
