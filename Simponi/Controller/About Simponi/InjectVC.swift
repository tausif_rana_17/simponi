//
//  InjectVC.swift
//  Simponi
//
//  Created by suresh on 13/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit

class InjectVC: UIViewController , UITableViewDelegate, UITableViewDataSource , UIGestureRecognizerDelegate {

    var arrItems: Array<Any> = []
    let cellSpacingHeight: CGFloat = 3
    
    
    
    @IBOutlet weak var lblDosingClick: UILabel!
    @IBOutlet weak var lblStorageClick: UILabel!
    @IBOutlet weak var lblSafetyClick: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 132.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none

        arrItems = [ "Simponi SmartJect Pen", "Simponi Pre-Filled Syringe", "10 Tips for Self-Injecting with the SmartJect Pen","10 Tips for Self-Injecting with the Pre-Filled Pen"];
    
        let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(InjectVC.lblDosingClickTap))
        lblDosingClick.isUserInteractionEnabled = true
        lblDosingClick.addGestureRecognizer(gestureRecognizer1)
        
        
        let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(InjectVC.lblStorageClickTap))
        lblStorageClick.isUserInteractionEnabled = true
        lblStorageClick.addGestureRecognizer(gestureRecognizer2)
        
        let gestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(InjectVC.lblSafetyClickTap))
        lblSafetyClick.isUserInteractionEnabled = true
        lblSafetyClick.addGestureRecognizer(gestureRecognizer3)

    }
    
    @objc func lblDosingClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.DosingVC) as! DosingVC
        self.present(loginVC, animated: true, completion: nil)
    }
    
    @objc func lblStorageClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.StorageDisposalVC) as! StorageDisposalVC
        self.present(loginVC, animated: true, completion: nil)
    }
    
    @objc func lblSafetyClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.SafetyVC) as! SafetyVC
        self.present(loginVC, animated: true, completion: nil)
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
  /* func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }*/
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
     
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.layer.shadowOpacity = 0.5
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        
        let str = arrItems[indexPath.section] as? String
        cell.lblName?.text = str
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("You tapped cell number \(indexPath.section).")
        
        if indexPath.section == 0
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: "InjectDetailSubviewID") as! InjectDetailSubview
            
            self.present(loginVC, animated: true, completion: nil)
        }
        else if indexPath.section == 1
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.PsoriaticVC) as! PsoriaticVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        else
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.PsoriaticVC) as! PsoriaticVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
    }
    
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
  
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
