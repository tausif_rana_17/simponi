//
//  StorageDisposalVC.swift
//  Simponi
//
//  Created by suresh on 15/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit

class StorageDisposalVC: UIViewController  {

    @IBOutlet weak var imgView: UIImageView!

    @IBOutlet weak var myPrefilledView: UIView!
    
    @IBOutlet weak var lblInjectClick: UILabel!
    @IBOutlet weak var lblDosingClick: UILabel!
    @IBOutlet weak var lblSafetyClick: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.imgView.layer.cornerRadius = 5
        self.imgView.layer.shadowColor = UIColor.gray.cgColor
        self.imgView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.imgView.layer.shadowOpacity = 0.3
        self.imgView.layer.borderColor = UIColor.lightText.cgColor
        self.imgView.layer.borderWidth = 1
        
        self.myPrefilledView.layer.cornerRadius = 3
        self.myPrefilledView.layer.borderColor = UIColor.lightGray.cgColor
        self.myPrefilledView.layer.borderWidth = 1
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.lblInjectClickTap))
        lblInjectClick.isUserInteractionEnabled = true
        lblInjectClick.addGestureRecognizer(gestureRecognizer)
        
        let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(self.lblDosingClickTap))
        lblDosingClick.isUserInteractionEnabled = true
        lblDosingClick.addGestureRecognizer(gestureRecognizer1)
        
        let gestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(self.lblSafetyClickTap))
        lblSafetyClick.isUserInteractionEnabled = true
        lblSafetyClick.addGestureRecognizer(gestureRecognizer3)
        
    }

    @objc func lblInjectClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.InjectVC) as! InjectVC
        
        self.present(loginVC, animated: true, completion: nil)
    }
    
    @objc func lblDosingClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.DosingVC) as! DosingVC
        self.present(loginVC, animated: true, completion: nil)
    }
    @objc func lblSafetyClickTap()
    {
        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.SafetyVC) as! SafetyVC
        self.present(loginVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
