//
//  MenuVC.swift
//  Simponi
//
//  Created by suresh on 10/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit
import Toast_Swift
import RSLoadingView
import QuartzCore

class MenuVC: UIViewController , UITableViewDelegate, UITableViewDataSource {

    var arrMenuName : [String] = ["Scan Simponi Barcode","My Condition","About Simponi","Helpful Videos","FAQs","Lifestyle","Member Support"]
    var style = ToastStyle()
    let Session = UserDefaults.standard
    let loadingView = RSLoadingView()
    var strWelcomeuser = ""
    let cellSpacingHeight: CGFloat = 3
    var getCustomerEmail = ""
    var alertVC: CPAlertVC!
    var alertView = AlertVw()
    var direction : String?
    var titleText : String?
    var descriptionText : String?
    var imgAlert : UIImage?

    
    @IBOutlet weak var lblWelcomeuser: UILabel!
    @IBOutlet weak var myTableview: UITableView!
    @IBOutlet weak var btnBarcodeOutlet: UIButton!
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        //self.loadingActivity()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        myTableview.delegate = self
        myTableview.dataSource = self
        
        getCustomerEmail = Session.string(forKey: Constants.SESSION.CUSTOMEREMAIL)!

        self.lblWelcomeuser.text = getCustomerEmail.uppercased()
        
        self.btnBarcodeOutlet.layer.cornerRadius = 5
        self.btnBarcodeOutlet.layer.shadowColor = UIColor.gray.cgColor
        self.btnBarcodeOutlet.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.btnBarcodeOutlet.layer.shadowOpacity = 0.3
        self.btnBarcodeOutlet.layer.borderColor = UIColor.lightText.cgColor
        self.btnBarcodeOutlet.layer.borderWidth = 1
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(MenuVC.tapFunction))
        lblWelcomeuser.isUserInteractionEnabled = true
        lblWelcomeuser.addGestureRecognizer(tap)
    }

    @objc func tapFunction(sender:UITapGestureRecognizer)
    {
        print("tap working")
        
        let alertVC = CPAlertVC.create().config(title: "Alert", message: "Are you sure to logout")
        
        alertVC.addAction(CPAlertAction(title: "Logout", type: .normal, handler: {
            
            self.Session.removeObject(forKey: Constants.SESSION.CUSTOMERID)
            self.Session.removeObject(forKey: Constants.SESSION.CUSTOMERNAME)
            self.Session.removeObject(forKey: Constants.SESSION.TOKENID)
            
            self.Session.synchronize()
            
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.LoginVC) as! LoginVC
            self.navigationController?.pushViewController(loginVC, animated: true)
            
        }))
        
        alertVC.addAction(CPAlertAction(title: "Cancel", type: .cancel, handler: {
        }))
        
        alertVC.show(into: self)
        
        
       
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMenuName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = myTableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell

        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.font = UIFont(name:"OpenSans-Semibold", size:15)
        cell.textLabel?.text = arrMenuName[(indexPath as NSIndexPath).row]
        
        return cell
        
    }
    
   /* func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath?
    {
        let cell = tableView.cellForRow(at: indexPath)
        
        cell?.backgroundColor = UIColor.gray
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?
    {
        let cell = tableView.cellForRow(at: indexPath);
        
        cell?.backgroundColor = UIColor.green
        
        return indexPath
    }*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50.0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //let indexPath = arrMenuName[(indexPath as NSIndexPath).row]
        
        if (indexPath.row == 0)
        {
            self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }
        else if (indexPath.row == 1)
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.MyConditionVC) as! MyConditionVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        else if (indexPath.row == 2)
        {
            
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.AboutSimponiVC) as! AboutSimponiVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        else if (indexPath.row == 3)
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.HelpfulvideosVC) as! HelpfulvideosVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        else if (indexPath.row == 4)
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.FAQVc) as! FAQVc
            self.navigationController?.pushViewController(loginVC, animated: true)
            
        }
        else if (indexPath.row == 5)
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.LifestyleVC) as! LifestyleVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        else if (indexPath.row == 6)
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.MembersupportVC) as! MembersupportVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
   
    @IBAction func btnBarcodeClick(_ sender: Any)
    {
        self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
    }
    
    func loadingActivity()
    {
        loadingView.mainColor = UIColor.blue
        loadingView.show(on: view)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
