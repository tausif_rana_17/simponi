//
//  ModelVC.swift
//  Simponi
//
//  Created by suresh on 16/3/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit

class ModelVC: UIViewController , UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    
    var arrItems: Array<Any> = []
    let cellSpacingHeight: CGFloat = 3
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 132.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        
        arrItems = [ "In this section you will find the answers to the most commonly asked questions about self-injecting and Simponi. If you have any questions that don't appear in the list below, please call the Simponi Helpline on 1800 936 257."];
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ModelCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let str = arrItems[indexPath.section] as? String
        cell.lblDetail?.text = str
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("You tapped cell number \(indexPath.section).")
       
    }


    @IBAction func btnBackClick(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
