//
//  MyConditionVC.swift
//  Simponi
//
//  Created by suresh on 16/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit

class MyConditionVC: UIViewController , UITableViewDelegate ,  UITableViewDataSource
{

    @IBOutlet weak var tableView: UITableView!
    
    var arrItems: Array<Any> = []
    let cellSpacingHeight: CGFloat = 8

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 132.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
       // arrItems = [ "Psoriatic Arthritis", "Pheumatoid Arthritis", "Ankylosing Spondylitis"];
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let str = arrItems[indexPath.section] as? String
        cell.lblName?.text = str
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("You tapped cell number \(indexPath.section).")
        
        if indexPath.section == 0
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.PsoriaticVC) as! PsoriaticVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        else if indexPath.section == 1
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.PsoriaticVC) as! PsoriaticVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        else
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.PsoriaticVC) as! PsoriaticVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
