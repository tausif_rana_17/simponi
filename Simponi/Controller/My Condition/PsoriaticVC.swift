//
//  PsoriaticVC.swift
//  Simponi
//
//  Created by suresh on 17/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit

class PsoriaticVC: UIViewController , UITableViewDelegate, UITableViewDataSource {

    let kHeaderSectionTag: Int = 6900;
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblAnkylosing: UILabel!
    @IBOutlet weak var lblRheumatoid: UILabel!
    @IBOutlet weak var lblFaqs: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 132.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        sectionNames = [ "\tWhat is PSA","\tWhat Cause PSA", "\tHow does Psoritis Arthritis Progress" ];
        sectionItems = [ ["Prostate Specific Antigen (PSA) is a protein produced exclusively by prostate cells. There is a simple blood test to measure your PSA level and this may help to detect early prostate cancer."],
                         ["PSA levels can rise for reasons related to the prostate, but there are actually more situations that are not directly related to prostate health that can cause PSA to rise. The factors in the latter category typically are temporary causes. Here are eight reasons why you may experience a rise in your PSA levels.\n\n1. Enlarged prostate\n2. Prostatitis\n3. Prostate cancer\n4. Recent sexual activity\n5. Urinary tract infection"],
                         ["Everyone is different, and PSA, which is complex and unpredictable condition, can affect people in different wavs."]
        ];
        self.tableView!.tableFooterView = UIView()
        
        self.imgView.layer.cornerRadius = 3
        self.imgView.layer.shadowColor = UIColor.gray.cgColor
        self.imgView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.imgView.layer.shadowOpacity = 0.3
        self.imgView.layer.borderColor = UIColor.lightText.cgColor
        self.imgView.layer.borderWidth = 1
        
        
        
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.lblAnkylosingClickTap))
        lblAnkylosing.isUserInteractionEnabled = true
        lblAnkylosing.addGestureRecognizer(gestureRecognizer)
        
        let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(self.lblRheumatoidClickClickTap))
        lblRheumatoid.isUserInteractionEnabled = true
        lblRheumatoid.addGestureRecognizer(gestureRecognizer1)
        
        
        let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(self.lblFaqsClickTap))
        lblFaqs.isUserInteractionEnabled = true
        lblFaqs.addGestureRecognizer(gestureRecognizer2)
    }
    
    @objc func lblAnkylosingClickTap()
    {
       
    }
    
    @objc func lblRheumatoidClickClickTap()
    {
        
    }
    
    @objc func lblFaqsClickTap()
    {
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = "Retrieving data.\nPlease wait."
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = .center;
            messageLabel.font = UIFont(name: "HelveticaNeue", size: 15.0)!
            messageLabel.sizeToFit()
            self.tableView.backgroundView = messageLabel;
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            return self.sectionNames[section] as? String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.colorWithHexString(hexStr: "#2F5173")
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont(name: "OpenSans-Semibold", size: 15)!

        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        //let headerFrame = self.view.frame.size
        // let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 32, y: 13, width: 18, height: 18));
        let theImageView = UIImageView(frame: CGRect(x: 5, y: 13, width: 18, height: 18));
        
        theImageView.image = UIImage(named: "Chevron-Dn-Wht")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(FAQVc.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        // return 111
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.textLabel?.font = UIFont(name: "OpenSans-Semibold", size: 15)!
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.text = section[indexPath.row] as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
         tableView.tableHeaderView?.backgroundColor = UIColor.red
        

    }
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tableView!.beginUpdates()
            self.tableView!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
