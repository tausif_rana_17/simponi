//
//  LifestyleVC.swift
//  Simponi
//
//  Created by suresh on 19/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit
import Toast_Swift

class LifestyleVC: UIViewController {

    
    var arrItems: Array<Any> = []
    var style = ToastStyle()
    var strPatientType = ""
    var arrGastroID : Array<Any> = []
    var arrGastroName : Array<Any> = []
    var arrRheumID : Array<Any> = []
    var arrRheumName : Array<Any> = []

    let Session = UserDefaults.standard
    let cellSpacingHeight: CGFloat = 5
    
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var myImageView: UIImageView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        strPatientType =  (Session.string(forKey: Constants.SESSION.PATIENT_TYPE))!
            
        if (strPatientType == Constants.SESSION.GASTRO)
        {
            arrGastroID = [ "65", "66", "67", "68"]
            arrGastroName = [ "Mind", "Emotions", "Diet", "Pysical"]


        }
        else
        {
            arrRheumID = [ "6", "7", "8", "10", "62", "61"]
            arrRheumName = [ "Mind, Body and Spirit", "Eating Well", "Staying Active", "Tried &  Tested",  "Find Recipes", "Read more about eating well"]

        }
        
        self.myImageView.layer.shadowColor = UIColor.gray.cgColor
        self.myImageView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.myImageView.layer.shadowOpacity = 0.3
        self.myImageView.layer.borderColor = UIColor.lightText.cgColor
        self.myImageView.layer.borderWidth = 1
        
        tableView.estimatedRowHeight = 132.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()

    }

    @IBAction func btnBackClick(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}


// Tableview Datasource & Delegates with Extension

extension LifestyleVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        //return arrItems.count
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return 1

        }
        
        if (strPatientType == Constants.SESSION.GASTRO)
        {
            return arrGastroName.count
        }
        else
        {
            return arrRheumName.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! DetailCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none

            cell.lblDetail?.text = Constants.Description.LIFESTYLE_NOTE
            
            return cell
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomCell
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.myDisplayview.layer.cornerRadius = 5
            cell.myDisplayview.layer.masksToBounds = true
            cell.myDisplayview.layer.shadowColor = UIColor.gray.cgColor
            cell.myDisplayview.layer.shadowOffset = CGSize(width: -1, height: 1)
            cell.myDisplayview.layer.shadowOpacity = 0.5
            cell.myDisplayview.layer.borderColor = UIColor.lightGray.cgColor
            cell.myDisplayview.layer.borderWidth = 1.5
            
           
            if (strPatientType == Constants.SESSION.GASTRO)
            {
                let str = arrGastroName[indexPath.row] as? String
                cell.lblName?.text = str
            }
            else
            {
                let str = arrRheumName[indexPath.row] as? String
                cell.lblName?.text = str
            }

            
            return cell
        }
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.section == 0
        {
            
        }
        else
        {
            print("You tapped cell number \(indexPath.row).")

            if (strPatientType == Constants.SESSION.GASTRO)
            {
                let str = arrGastroID[indexPath.row] as? String
                print("Selected Gestro Id \(str)")
            }
            else
            {
                let str = arrRheumID[indexPath.row] as? String
                print("Selected Rheum Id \(str)")
            }
            
        }
        
       /* if indexPath.section == 0
        {
            self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }
        else if indexPath.section == 1
        {
            self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }
        else if indexPath.section == 2
        {
            self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }
        else if indexPath.section == 3
        {
            self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }*/
    }
    
    
    /* func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     return cellSpacingHeight
     }
     
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     let headerView = UIView()
     headerView.backgroundColor = UIColor.clear
     return headerView
     }*/

}
