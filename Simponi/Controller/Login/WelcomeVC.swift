//
//  WelcomeVC.swift
//  Simponi
//
//  Created by suresh on 9/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit
import KVNProgress
import Alamofire
import SwiftyJSON
import Toast_Swift

class WelcomeVC: BaseVC {

    let Session = UserDefaults.standard

    @IBOutlet weak var myDisplay: UIView!
    @IBOutlet weak var btnLoginOutlet: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        
        if checkInternet() == true
        {
            if (Session.string(forKey: Constants.SESSION.CUSTOMERID) != nil)
            {
                let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.MenuVC) as! MenuVC
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }
        else
        {
            showNormalAlert(strMessage: Constants.Keys.INTERNET_UNAVAILABLE)
        }
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.myDisplay.layer.cornerRadius = 5
        self.myDisplay.layer.shadowColor = UIColor.gray.cgColor
        self.myDisplay.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.myDisplay.layer.shadowOpacity = 0.3
        self.myDisplay.layer.borderColor = UIColor.lightText.cgColor
        self.myDisplay.layer.borderWidth = 1

        self.btnLoginOutlet.layer.cornerRadius = 3
        self.btnLoginOutlet.layer.shadowColor = UIColor.white.cgColor
        self.btnLoginOutlet.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.btnLoginOutlet.layer.shadowOpacity = 0.2
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
