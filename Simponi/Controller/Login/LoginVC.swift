//
//  LoginVC.swift
//  Simponi
//
//  Created by suresh on 9/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit
import KVNProgress
import Alamofire
import SwiftyJSON
import Toast_Swift
import EZLoadingActivity


class LoginVC: BaseVC , UITextFieldDelegate{

    
    // Declaration ====================
    let Session = UserDefaults.standard
    var objResponseClass = ResponseClass()
    
    
    // Outlets ====================
    @IBOutlet weak var myDisplay: UIView!
    @IBOutlet weak var btnLoginOutlet: UIButton!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var myUserview: UIView!
    @IBOutlet weak var myPasswordview: UIView!
    @IBOutlet weak var btnSavepasswordOutlet: UIButton!
    
    // Session Object ====================
    let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        
        return SessionManager(configuration: configuration)
    }()
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        
        if checkInternet() == true
        {
            if (Session.string(forKey: Constants.SESSION.CUSTOMERID) != nil)
            {
                let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.MenuVC) as! MenuVC
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }
        else
        {
            showNormalAlert(strMessage: Constants.Keys.INTERNET_UNAVAILABLE)
        }
    }

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.myDisplay.layer.cornerRadius = 5
        self.myDisplay.layer.shadowColor = UIColor.gray.cgColor
        self.myDisplay.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.myDisplay.layer.shadowOpacity = 0.3
        self.myDisplay.layer.borderColor = UIColor.lightText.cgColor
        self.myDisplay.layer.borderWidth = 1
        
        self.myUserview.layer.cornerRadius = 5
        self.myUserview.layer.shadowColor = UIColor.gray.cgColor
        self.myUserview.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.myUserview.layer.shadowOpacity = 0.3
        self.myUserview.layer.borderColor = UIColor.lightText.cgColor
        self.myUserview.layer.borderWidth = 1
        
        self.myPasswordview.layer.cornerRadius = 5
        self.myPasswordview.layer.shadowColor = UIColor.gray.cgColor
        self.myPasswordview.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.myPasswordview.layer.shadowOpacity = 0.3
        self.myPasswordview.layer.borderColor = UIColor.lightText.cgColor
        self.myPasswordview.layer.borderWidth = 1
        
        self.btnLoginOutlet.layer.cornerRadius = 3
        self.btnLoginOutlet.layer.shadowColor = UIColor.white.cgColor
        self.btnLoginOutlet.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.btnLoginOutlet.layer.shadowOpacity = 0.2
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        txtUsername.delegate = self
        txtPassword.delegate = self

    }

    
    @IBAction func btnSavepasswordClick(_ sender: Any)
    {
        if AppDelegate.menu_bool == false
        {
            AppDelegate.menu_bool = true
            btnSavepasswordOutlet.setImage(UIImage(named: "checked.png"), for: .normal)

        }
        else
        {
            AppDelegate.menu_bool = false
            btnSavepasswordOutlet.setImage(UIImage(named: "unchecked.png"), for: .normal)

        }
    }
    
    @IBAction func btnLoginClick(_ sender: Any)
    {
       
        if checkInternet() == false {
            showNormalAlert(strMessage: Constants.Keys.INTERNET_UNAVAILABLE)
            return
        }
        
        if (txtUsername.text == "")
        {
            showNormalAlert(strMessage: Constants.Messages.EMAIL_REQUIRED)
        }
        else if (txtPassword.text == "")
        {
            showNormalAlert(strMessage: Constants.Messages.PASSWORD_REQUIRED)
        }
        else
        {
          /*  let urlParams = [
                "username": "patient_uc@jemstone.ie",
                "password": "Uc-simpon!"
                ]*/
            EZLoadingActivity.show("Please Wait...", disableUI: true)

            let urlParams = [
                "username": txtUsername.text!,
                "password": txtPassword.text!
            ]
            print(urlParams)
            
            sessionManager.request(Constants.URLS.loginURL, method: .post, parameters: urlParams, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch response.result
                {
                case .success:
                    
                    let responseJSON = response.result.value as! [String:AnyObject]
                    print(responseJSON)
                    let JsonData = responseJSON["Data"] as! [String:AnyObject]
                    let responseCode = self.objResponseClass.JSON_RESPONSE_CODE(ResponseCode: JsonData["response_code"] as! Int,ReponseMessage: JsonData["message"] as! String)
                    
                    //let responseCode = JsonData["response_code"] as! Int
                     //print(responseCode)
                   
                   
                    
                    if responseCode == 200
                    {
                        EZLoadingActivity.hide()
                        let arrRecords = JsonData["records"] as! [String:AnyObject]
                        let customerID = arrRecords["user_id"] as! Int
                        let CustomerEmailID = arrRecords["username"] as! String
                        let loginToken = arrRecords["token"] as! String
                        let patientType = arrRecords["patient_type"] as! String

                        print(customerID)
                        print(CustomerEmailID)
                        print(loginToken)
                        print(patientType)

                        
                        if AppDelegate.menu_bool == false
                        {
                            
                           // self.Session.set(customerID, forKey: Constants.SESSION.CUSTOMERID)
                            self.Session.set(CustomerEmailID, forKey: Constants.SESSION.CUSTOMEREMAIL)
                            self.Session.set(loginToken, forKey: Constants.SESSION.TOKENID)
                            self.Session.set(patientType, forKey: Constants.SESSION.PATIENT_TYPE)

                            
                            self.Session.synchronize()
                        }
                        else
                        {
                            AppDelegate.menu_bool = false
                            
                            self.Session.set(customerID, forKey: Constants.SESSION.CUSTOMERID)
                            self.Session.set(CustomerEmailID, forKey: Constants.SESSION.CUSTOMEREMAIL)
                            self.Session.set(loginToken, forKey: Constants.SESSION.TOKENID)
                            self.Session.set(patientType, forKey: Constants.SESSION.PATIENT_TYPE)
                            self.Session.synchronize()
                        }
                        
                       EZLoadingActivity.hide()
                        
                        let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.MenuVC) as! MenuVC
                        self.navigationController?.pushViewController(loginVC, animated: true)
                    }
                    if responseCode == 400
                    {
                        EZLoadingActivity.hide()
                        self.showNormalAlert(strMessage: JsonData["message"] as! String)
                        
                    }
                    
                case .failure(let error):
                    
                    print(error)
                    EZLoadingActivity.hide()
                }
            }
        
        }
                
                
                
          /*  let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.MenuVC) as! MenuVC
            
            loginVC.strWelcomeuser = txtUsername.text!
            
            self.navigationController?.pushViewController(loginVC, animated: true)*/
    
        
        
        
        //  let urlParams: Parameters = ["email": "bhagat.sharma@jemstone.ie"]
        
        //let urlParams: Parameters = ["username": "patient_uc@jemstone.ie","password": "Uc-simpon!"]
        
       
        
      /*  let urlParams = [
            "username": "patient_uc@jemstone.ie",
            "password": "Uc-simpon!",
        ]
        
        Alamofire.request("https://www.simponivitality.ie/index.php/api/login", method: .post, parameters: urlParams, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch response.result
            {
            case .success:
                
                let responseJSON = response.result.value as! [String:AnyObject]
                print(responseJSON)
                let JsonData = responseJSON["Data"] as! [String:AnyObject]
                
                let responseCode = JsonData["response_code"] as! Int
                print(responseCode)

                let arrRecords = JsonData["records"] as! [String:AnyObject]
                let patient_type = arrRecords["patient_type"] as? String
                print(patient_type)

            case .failure(let error):
                
                print(error)
                
            }
        }
    */
        
      //   let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.MenuVC) as! MenuVC
      //   self.navigationController?.pushViewController(loginVC, animated: true)
        
        
        
      /*   if checkInternet() == false {
            showNormalAlert(strMessage: Constants.Keys.INTERNET_UNAVAILABLE)
            return
        }
        
        if (txtUsername.text == "")
        {
            showNormalAlert(strMessage: "Please enter user name")

        }
        else if (txtPassword.text == "")
        {
            showNormalAlert(strMessage: "Please enter password")
        }
        else
        {
            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuVCID") as! MenuVC
            
            loginVC.strWelcomeuser = txtUsername.text!

            self.navigationController?.pushViewController(loginVC, animated: true)
        }*/
        
       /* if !Validator.required().apply((self.txtEmail.text!)) {
            showNormalAlert(strMessage: "Please provide username or email address!")
        }*/
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        txtUsername.resignFirstResponder()
        txtPassword.resignFirstResponder()
        
        return true;
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
