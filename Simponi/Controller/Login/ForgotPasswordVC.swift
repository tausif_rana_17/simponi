//
//  ForgotPasswordVC.swift
//  Simponi
//
//  Created by suresh on 14/3/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit
import KVNProgress
import Alamofire
import SwiftyJSON
import Toast_Swift
import EZLoadingActivity

class ForgotPasswordVC: BaseVC , UITextFieldDelegate {

    
    @IBOutlet weak var txtEmailID: UITextField!
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    var objResponseClass = ResponseClass()

    @IBOutlet weak var myView: UIView!
    
    @IBOutlet weak var myView1: UIView!
    var alertView = AlertVw()
    var direction : String?
    var titleText : String?
    var descriptionText : String?
    var imgAlert : UIImage?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        self.btnSubmitOutlet.layer.cornerRadius = 3
        self.btnSubmitOutlet.layer.shadowColor = UIColor.white.cgColor
        self.btnSubmitOutlet.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.btnSubmitOutlet.layer.shadowOpacity = 0.2
        
        self.myView.layer.cornerRadius = 5
        self.myView.layer.shadowColor = UIColor.gray.cgColor
        self.myView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.myView.layer.shadowOpacity = 0.3
        self.myView.layer.borderColor = UIColor.lightText.cgColor
        self.myView.layer.borderWidth = 1
        
        self.myView1.layer.cornerRadius = 5
        self.myView1.layer.shadowColor = UIColor.gray.cgColor
        self.myView1.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.myView1.layer.shadowOpacity = 0.3
        self.myView1.layer.borderColor = UIColor.lightText.cgColor
        self.myView1.layer.borderWidth = 1
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        txtEmailID.delegate = self

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnSubmitClick(_ sender: Any)
    {
        if checkInternet() == false {
            showNormalAlert(strMessage: Constants.Keys.INTERNET_UNAVAILABLE)
            return
        }
        
        if (txtEmailID.text == "")
        {
            showNormalAlert(strMessage: "Please enter email id")
        }
        else
        {
            EZLoadingActivity.show("Please Wait...", disableUI: true)

            let urlParams = [
                "email": txtEmailID.text!,
            ]
            print(urlParams)
            
            Alamofire.request(Constants.URLS.forgotPassword, method: .post, parameters: urlParams, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch response.result
                {
                case .success:
                    
                    let responseJSON = response.result.value as! [String:AnyObject]
                    print(responseJSON)
                    let JsonData = responseJSON["Data"] as! [String:AnyObject]
                    let responseCode = self.objResponseClass.JSON_RESPONSE_CODE(ResponseCode: JsonData["response_code"] as! Int,ReponseMessage: JsonData["message"] as! String)
                    print(responseCode)

                    if responseCode == 200
                    {
                        EZLoadingActivity.hide()
                        self.txtEmailID.text = ""
                        self.showNormalAlert(strMessage: JsonData["message"] as! String)
                    }
                  /*  if responseCode == 400
                    {
                        EZLoadingActivity.hide()
                        self.showNormalAlert(strMessage: JsonData["message"] as! String)

                    }*/
              
                case .failure(let error):
                    
                    print(error)
                    EZLoadingActivity.hide()
                }
            }
            
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        txtEmailID.resignFirstResponder()
        
        return true;
    }
}
