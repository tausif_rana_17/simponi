//
//  MembersupportVC.swift
//  Simponi
//
//  Created by suresh on 20/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit
import Toast_Swift

class MembersupportVC: UIViewController , UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
   
    var arrItems: Array<Any> = []
    let cellSpacingHeight: CGFloat = 3
    var style = ToastStyle()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 132.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        
        arrItems = [ "Nurse Visit", "Shaprs Bin","Travel Pack", "Arthritis Ireland", "Helpline", "Text Reminder", "Phone Reminder"];
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.myDisplayview.layer.cornerRadius = 5
        cell.myDisplayview.layer.masksToBounds = true
        cell.myDisplayview.layer.shadowColor = UIColor.gray.cgColor
        cell.myDisplayview.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.myDisplayview.layer.shadowOpacity = 0.5
        cell.myDisplayview.layer.borderColor = UIColor.lightGray.cgColor
        cell.myDisplayview.layer.borderWidth = 1
        
        let str = arrItems[indexPath.section] as? String
        cell.lblName?.text = str
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("You tapped cell number \(indexPath.section).")
        
        if indexPath.section == 0
        {
            self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }
        else if indexPath.section == 1
        {
            self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }
        else if indexPath.section == 2
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.TravelpackVC) as! TravelpackVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        else if indexPath.section == 3
        {
           self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }
        else if indexPath.section == 4
        {
           self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }
        else if indexPath.section == 5
        {
            let loginVC = UIStoryboard(name: Constants.STORYBOARD.MAIN, bundle: nil).instantiateViewController(withIdentifier: Constants.STORYBOARD.CLASS_NAME.TextreminderVC) as! TextreminderVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        else
        {
            self.view.makeToast("Comming Soon", duration: 3.0, position: .bottom, style: self.style)
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
