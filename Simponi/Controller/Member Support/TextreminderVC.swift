//
//  TextreminderVC.swift
//  Simponi
//
//  Created by suresh on 21/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit

class TextreminderVC: UIViewController
{
    @IBOutlet weak var btnVideoOutlet: UIButton!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.btnVideoOutlet.layer.cornerRadius = 3
        self.btnVideoOutlet.layer.shadowColor = UIColor.white.cgColor
        self.btnVideoOutlet.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.btnVideoOutlet.layer.shadowOpacity = 0.2
    }

    
    @IBAction func btnBackClick(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
