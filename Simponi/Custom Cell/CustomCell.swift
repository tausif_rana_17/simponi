//
//  CustomCell.swift
//  Simponi
//
//  Created by suresh on 13/2/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var myImageview: UIImageView!
    
    @IBOutlet weak var myDisplayview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
