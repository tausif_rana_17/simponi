//
//  ModelCell.swift
//  Simponi
//
//  Created by suresh on 16/3/18.
//  Copyright © 2018 Bhavi. All rights reserved.
//

import UIKit

class ModelCell: UITableViewCell {

    
    @IBOutlet weak var lblDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
